jQuery( document ).ready(function($) {				
	//Makes an H1 tag say "WHOA" when clicked..
	//this is "event binding".
	//this is passing a function as an argument...

	$("h1").click(function() {
      	this.innerHTML = "WHOA!";
    });
	
	$("#testAbout").click(function() {
		this.innerHTML = "Butt";
	});
	
	//Removes all elements from the div prior to reloading new content. 
	$("#leftNavItems li").click(function() {
		$("#divContent").empty();
	});
	
	$("#leftNavItems #leftAboutMe").click(function() {
		$("#divContent").load("aboutMe.html");
	});
	
	$("#leftNavItems #leftProjects").click(function() {
		$("#divContent").load("sideProjects.html");
	});
});
