<?php
//require_once('controllers/welcomemenu.php');

class Welcome extends CI_Controller {
	
	function __construct() {
		parent::__construct();	
	}
	
	function index() {
		
		$this->load->helper('url');
		
		//echo 'this is the Welcome index function';
		//$data['clienttestdata'] = 'data from welcome.php!';
		$data['menu_items'] = array('About', 'Portfolio', 'Resume', 'Fun', 'Blog');
		$data['welcomemenu'] = $this->load->view('welcome/welcomemenu', $data, true);
		
		$this->load->view('templates/header');
		$this->load->view('pages/welcome', $data);
		$this->load->view('templates/footer');
	}
}