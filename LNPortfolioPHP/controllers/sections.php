<?php  
class Sections extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	
	function index($data = 'about') {
		/*$this->load->helper('url');

		$outdata['test'] = 'Test of outdata';
		$outdata['inputtest'] = $data;
		$outdata['sectContent'] = $this->load->view('sections/'.$data, '', true);
				
		
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');*/
	}
	
	function about() {
		$this->load->helper('url');
		$data = array();
		$outdata['sectContent'] = $this->load->view('sections/about', $data, true);
		
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');
	}
	
	function portfolio() {
		$this->load->helper('url');
		$data = array();
		$outdata['sectContent'] = $this->load->view('sections/portfolio', $data, true);
		
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');
	}
	
	function resume() {
		$this->load->helper('url');
		$data = array();
		$outdata['sectContent'] = $this->load->view('sections/resume', $data, true);
		
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');
	}
	
	function fun() {
		$this->load->helper('url');
		$data = array();
		$outdata['sectContent'] = $this->load->view('sections/fun', $data, true);
		
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');
	}
	
	function blog() {
		$this->load->helper('url');
		$data = array();
		$outdata['sectContent'] = $this->load->view('sections/blog', $data, true);
	
		$this->load->view('templates/header');
		$this->load->view('pages/sections', $outdata);
		$this->load->view('templates/footer');
	}
}
