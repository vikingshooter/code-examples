<!DOCTYPE HTML>
<html class="main">
	<head>
		<title>
			Lewis Notestine
		</title>
		<link rel="icon" href="<?=base_url();?>assets/img/circleIcon.png"/>
		<link rel="image_src" href="<?php echo base_url();?>assets/img/fbImage.png"/>
		<meta property="og:title" content="Lewis Notestine, Developer & Data Analyst"/>
		<meta property="og:url" content="http://lewisnotes.com/"/>
		<meta property="og:image" content="http://lewisnotes.com/assets/img/fbImage.gif"/>
		
		<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery-ui-1.10.1.custom.min.css" />
		<LINK href="<?=base_url()?>assets/css/styleSheet.css" rel="stylesheet" type="text/css"/>
		<link href='http://fonts.googleapis.com/css?family=Carrois+Gothic|Carrois+Gothic+SC' rel='stylesheet' type='text/css'/>
	</head>
	<body>
	

