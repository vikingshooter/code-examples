		<section id="footer", class="footer">
			<div><strong>&copy; <?php echo date('Y')?> Lewis Notestine</strong></div>
			<div><?php echo safe_mailto('lewis@lewisnotes.com','contact Lewis')?></div>	
			<div>"Earth" image credit: NASA</div>
		</section>
		<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
		<script> 
			var $ = jQuery.noConflict();
		</script>
		<script src="<?php echo base_url();?>assets/js/jquery-ui-1.10.1.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/main.js"></script>
		<script src="<?php echo base_url();?>assets/js/sections.js"></script>

	</body>
</html>