		<section id="allWelcomeContent" class="mainBody">
			<header id="mainPageHdr" class=mainPageHdr>
				<div id="imgAndText">
					<img id="imgEarth" src="<?php echo base_url();?>assets/img/earth.jpg" class="hdrImg"></img>
					<div id="divNameTitle">
						<table id="tblNameTitle" class="titleText"> 
							<tr>
								<td id="tclTitleName" class="titleName">
									<span class="txtFName">Lewis</span><span class="txtLName">NOTESTINE</span>
								</td>
							</tr>
							<tr>
								<td id="tclTitleOccupation" class="titleOccupation">Developer, Data Analyst</td>						
							</tr>
						</table> 
					</div>
				</div>
			</header>
			
			<hr id="sectionsHeaderLine" class="sectionsHeaderLine"></hr>
			<div id="hoverMenu" class="sectionsMenuGraphic">
				<?php foreach(array('About', 'Portfolio', 'Resume', 'Fun', 'Blog') as $hItem):?>
				<div onclick='redirectTo(<?php echo ('"'.strtolower($hItem).'"');?>);' class='sectionsCircleImgDiv <?php echo $hItem?>'>
					<div class='sectionsCircleImg'>						
						<h1 class='sectionsCircleText'> 
							<?php echo $hItem;?>					
						</h1>	
					</div>
				</div>
				<?php endforeach;?>
			</div>
			<?php echo $sectContent;?>
		</section>
		<script src="<?php echo base_url();?>assets/js/sections.js"></script>		