<section id="sectMainMenuCircles">
	<div id='divMenuItems' class='menuGraphic'>
		<hr id='circleLine'></hr>
		 
		<?php foreach ($menu_items as $item):?>
			<div onclick='redirectTo(<?php echo ('"'.base_url()."index.php/sections/".strtolower($item).'"');?>);' class='circleImgDiv <?php /*this is to customize formatting for each menu item...*/echo strtolower($item);?>'>
				<div class='circleImg'>
					<h1 class='circleText'> 
						<?php echo $item;?>
					</h1>	
				</div>
			</div>
		<?php endforeach;?>

	</div>
</section>
