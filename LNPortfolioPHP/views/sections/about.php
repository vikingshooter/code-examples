<section id="aboutContent">
	<h1>About Lewis</h1>
	<article>
	
		<img class="imageList smallImage" src="<?php echo base_url();?>assets/img/lnPortrait.gif">
		<p>Having been a technical and analytical professional since 2004, I've been fortunate to have had a broad base of experience.  In my career, I've worked on projects entailing academic research, statistical analysis, market research, speech recognition, database administration, BI reporting, software development, web development, mobile/game development, and project management.</p>  
		<p>My core career interests include software engineering, statistical analysis, AI systems (both "strong" and practical), cognitive science, predictive modeling, machine translation/machine learning, and artificial neural networks. These interests have been served well by my graduate degree in Linguistics as well as my ongoing education in computer science.</p>  
		<p>I envision a future in which technology pushes the boundaries of human life, and my main career goal is to manifest that vision by helping to implement realistic extensions of technology and taking practical steps to make technology work better for humans in real-life situations. </p>
		<p>In my small free time I enjoy working on cool projects involving soldering, microcontrollers and electronics, or else wrenching on bicycles or anything else with moving or electronic parts.  I write code for fun whenever I can, and so far it hasn't gotten old!</p>
		<p>I am always excited to hear about new projects and opportunities to apply my particular skill set.  If you have any questions at all for me, please don't hesitate to check out my <?php echo anchor("sections/portfolio", "portfolio");?> or <?php echo safe_mailto('lewis@lewisnotes.com','contact me')?>.</p>	
	</article>
	
	
	<h1>About This Site</h1>	
	<article>
		<p>This site serves as a portal for all parties interested in my work, a place for friends and those with shared interests to contact me, a reference for  colleagues in the software/tech community in Portland, Oregon, and other great stuff.</p>
		<p>This site was created using the <?php echo anchor("http://ellislab.com/codeigniter","CodeIgniter")?> MVC framework for PHP, and uses HTML5, CSS3 and jQueryUI.  It's meant as an expression of my competence, vision and values, and of the focus and quality I put into all of my projects.</p>  
	
	</article>
</section>	