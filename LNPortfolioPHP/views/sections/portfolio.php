
<section id="portfolio content">
	<h1>Selected work</h1>
	<article id="portfolioIntro">
		<p>Thanks for taking the time to review some of my featured work! You can view specific examples of work in each of my core competencies by clicking each topic header below.<br/><br/>
		This page profiles selected work for: the <?php echo anchor("http://cahmi.org", "Child and Adolescent Health Measurement Initiative");?> at OHSU (my current employer), <?php echo anchor("http://dmacorporation.com", "DMA");?>,  and <?php echo anchor("http://avantassessment.com", "Avant Assessment")?>, as well as my academic work.  I'm proud of everything in this section and am happy to answer any questions about work details, technologies, statistics, and other details when they are available. <br/><br/>
		In some cases, details may be proprietary or confidential and thus not included here, but source code is given where possible with links to <a href="https://github.com/lewisNotestine">GitHub</a>.
		</p>
	</article>
	
	<div id="accordionContainer">
		<div id="accordion">
			<h2>Statistical analysis/modeling</h2>
			<div>
				<article>
					<img class="imageList" src="<?php echo base_url();?>assets/img/statsRoc.gif">
					<!--<img class="imageList" src="<?php echo base_url();?>assets/img/statsSurv.gif">-->
					
					<p>Several of my projects at <a href="http://dmacorporation.com/">DMA</a> involved developing and implementing <a href="http://dmacorporation.com/Research%20And%20Analytic%20Program%20Web%20Site%20Blurbs/Relationship%20Modeling%20Technologies(1).php">predictive models</a> that would predict likelihood of each of a list of product categories that a customer household would be most likely to next add to their banking portfolio, or if a household is 'at risk' of dropping a product or service.</p>
					<p>We implemented these models in database marketing programs which helped clients target their advertising more effectively and increase cross-sell and ROI for their own unique customer bases.</p>	
					<p>These were really exciting, challenging projects that required searching academic journals, intricate data management solutions, and the fitting and re-fitting of models to find those with the best characteristics.</p>
				</article>
			</div>
			
			<h2>Automated data management</h2>
			<div>
				<article>
					<img class="imageList" src="<?php echo base_url();?>assets/img/dpp2.gif">
					<img class="imageList" src="<?php echo base_url();?>assets/img/etl.gif">
					
					<p>The CAHMI's <a href="http://childhealthdata.org">Data Resource Center</a> is a valuable asset for epidemiologists, statisticians, health care professionals, policymakers and other interested parties in the field of child and family health care.</p>
					<p>Understandably, the DRC requires a lot of back-end data work.  To meet data management and production workflow needs, I wrote a .NET Windows Forms application in C# and VB.NET that uses the <a href="ftp://public.dhe.ibm.com/software/analytics/spss/documentation/statistics/20.0/en/netplugin/Manuals/Microsoft_NET_User_Guide_for_IBM_SPSS_Statistics.pdf">SPSS XD API</a> for .NET.</p>
					<p>This application automatically runs necessary statistical analyses, and automatically matches and uploads processed data to a SQL Server database.  </p> 
					<p>I also created several SQL Server Integration Services (SSIS) packages that allow research staff to test their data and view it on local IIS instances before publishing data to live websites.</p>
					<p>These tools have helped the CAHMI to drastically reduce errors, human effort and time spent running statistics and performing extract, transfer and load (ETL) operations.</p>
				</article>
			</div>
			
			<h2>Database design/development</h2>
			<div>
				<article>
					<img class="imageList" src="<?php echo base_url();?>assets/img/geoCode.gif">
					<img class="imageList" src="<?php echo base_url();?>assets/img/dbSchema.gif">
					
					<p>In my career, I've had to use a wide range of database technologies, including MS SQL Server (2005 - 2012), MySQL, Firebird/InterBase, Borland Paradox, SQLite, and MS Access.  I've had to develop database solutions for projects ranging from immense financial CRM databases, complex queries on health care statistics, to tiny databases embedded in mobile apps.</p>
					<p><a href="http://dmacorporation.com">DMA</a>, for example, uses sophisticated <a href="">database marketing programs</a> to help their clients grow profitably.</p>
					<p>Part of my role at the company was to support these programs by writing SQL Stored procedures and queries involving complex demographic, geographic/GIS, economic and financial data to help DMA's clients selectively target advertising efforts.</p>
					<p>These programs entailed address matching algorithms, complicated business rules and some creative querying!</p>
					
				</article>
				
			</div>
			
			<h2>Web development</h2>
			<div>
				<article>
					<a href="http://childhealthdata.org">
						<img class="imageList" src="<?php echo base_url();?>assets/img/chdScreen.gif">	
					</a>
					
					<a href="http://stagingwellvisitsurvey.com">
						<img class="imageList" src="<?php echo base_url();?>assets/img/phdsScreen.gif">	
					</a>
					
					<a href="http://staging.tccwellvisit.org">
						<img class="imageList" src="<?php echo base_url();?>assets/img/tccScreen.gif">
					</a>
				</article>
				<article>
					<p>The CAHMI operates a number of websites providing information on nationwide findings related to children's health care, as well as services for parents of young children and their health care providers.  These websites call for skills in ASP.NET, C#, VB.NET and SQL Server 2008 as well as the SiteFinity CMS and web standards such as HTML/XHTML, JavaScript and CSS.</p>
					<p>My contributions to the CAHMI's websites include updates and maintenance for the functionality, content, and database, as well as ongoing maintenance/DevOps, QA and other tasks.  I have contributed in these capacities to:</p>
					<ul>
						<li>
							<a href="http://childhealthdata.org">The Data Resource Center</a> for Child & Adolescent Health
						</li>
						<li>
							The <a href="http://wellvisitsurvey.com">Promoting Healthy Development Survey</a>, a tool for parents to provide feedback to their child's doctor <i>after</i> their child's well-visit
						</li>
						<li>
							The <a href="http://staging.tccwellvisit.org">Plan My Child's Well Visit</a> tool for parents to provide information to their child's doctor <i>before</i> their child's well-visit
						</li>
					</ul>				
				</article>
			</div>

			<h2>Mobile Development</h2>
			<div>
				<article>
					<p>Since 2011 I've been working as the principal programmer on a personal project with a few very talented friends including Michael Lorenzo of <a href="http://www.unrulyvegetables.com/">Unruly Vegetables Studio</a>.  Mike is a great level designer and artist and has worked on some great projects including <a href="http://www.playforgewar.com/">Forge</a>.</p>
					<p>The project is a classic shoot-em-up video game that we're building with <a href="http://coronalabs.com/">Corona SDK</a>, which is a great platform to work with. Look for more on this topic soon!</p>
				</article>
			</div>
			
			<h2>Business intelligence/reporting</h2>
			<div>
				<article>
					<img class="imageList" src="<?php echo base_url();?>assets/img/mhRptNew.gif">
					<img class="imageList" src="<?php echo base_url();?>assets/img/mhRptOld.gif">
					
					<p>I've had to develop BI reports using a number of technologies (Crystal Reports, SSIS, MS Access) for a wide range of applications. In each case the need to develop sophisticated ways of displaying dense, technical information in easily digestible packaging.  And, of course, one of the goals of Business Intelligence analytics is to make this kind of information available in real time and updateable.  </p>
					<p>To meet these needs for the CAHMI, I programmed and implemented a number of <a href="http://childhealthdata.org/docs/medical-home/2009-10-mhreports_or-738.pdf">Medical Home Performance profiles</a>, which I created to given specifications using SQL Server Reporting Services.</p>
					<p>I also used SSRS to create a set of feedback reports that go along with the <a href="http://wellvisitsurvey.com">Promoting Healthy Development Survey</a>, the goal of which is to give health care providers feedback on their quality improvement goals that is tailored to their own population of patients.</p>  
				</article>
			</div>
			
			<h2>Speech recognition/Artificial neural networks</h2>
			<div>
				<article>		
					<img class="imageList" src="<?php echo base_url();?>assets/img/spectrum.gif">
			
					<p>In a very interesting project for <a href="http://www.avantassessment.com/">Avant Assessment</a>, I helped to implement a working model for speech recognition in Limited English Proficiency students in the process of learning English.  </p>
					<p>One of the biggest challenges of training an artificial neural network model is setting different input parameters for training.  Young people learning English come from any number of diverse linguistic backgrounds, so a model has to be generalized enough to recognize stimuli that are phonetically very different, yet sensitive enough to recognize both grammatically correct patterns and errors.</p>
					<p>My role in this project was to perform acoustic measurements of real language stimuli and use these measurements to help train a stochastic artificial neural network model to recognize several words in some given phrases of English.  This was an excellent project to be a part of and was an ambitious undertaking in the area of language learning assessment.</p>
				</article>
			</div>
			 
			<h2>Academic work</h2>
			<div>
				<article>
					
					<p>The quest for knowledge never ends, nor should it.  My education has been broad and continues to expand.  My graduate education is in linguistics and psychology, with an emphasis on cognitive aspects of language storage and processing. I have also studied cognitive neuroscience and I am an fan of artificial intelligence and machine learning.</p>
					<p>I have included a copy of my <a href="<?php echo base_url();?>assets/doc/LN_Thesis_2007.pdf">M.A. Thesis</a>, submitted to the Linguistics Department and Graduate School at the University of Oregon.  I also presented my M.A. thesis work at the <a href="http://linggraduate.unm.edu/conference/2006/Abstracts%20Web%20Version/Notestine&Redford.htm">High Desert Linguistics Society Conference</a> in November 2006.</p>
					<p>In 2006 and 2007 I served as a Graduate Teaching Fellow, helping teach lower-division linguistics courses and aid professors in grading, etc.</p>
					<p>Since matriculating with a masters degree in linguistics in 2007 I have been continuing my education in computer science.  Examples of my subsequent work in this subject (posted with instructor permission) can be found on my <a href="https://github.com/lewisNotestine">GitHub profile</a>.</p>  
				</article>
			</div>
		</div>
	</div>
	

</section>	
		
	
	