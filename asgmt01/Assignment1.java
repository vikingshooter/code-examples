import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *	Assignment1.java
 *		Assignment 
 *		Demonstrates the DataParser class, which implements the collections framework and uses generics.
 *		DataParser takes data input and arranges successive input it to show patterns of how data 
 *		items follow other data items.  main() function is given by assignment, except for all calls to 
 *		DataParser objects
 *	Author:
 *	 	Lewis Notestine
 *	Date:
 *		10/13/2012
 *	References:
 *		On using interfaces with generics:
 *			http://stackoverflow.com/questions/5747597/comparable-and-generics?rq=1
 * */

// By default, this code will get its input data from the Java standard input,
// java.lang.System.in. To allow input to come from a file instead, which can be
// useful when debugging your code, you can provide a file name as the first
// command line argument. When you do this, the input data will come from the
// named file instead. If the input file is in the project directory, you will
// not need to provide any path information.
//
// In BlueJ, specify the command line argument when you call main().
//
// In Eclipse, specify the command line argument in the project's "Run Configuration."

public class Assignment1
{
	// returns an InputStream that gets data from the named file
	private static InputStream getFileInputStream(String fileName)
	{
		InputStream	inputStream;
		
		try {
			inputStream = new FileInputStream(new File(fileName));
			}
		catch (FileNotFoundException e) {			// no file with this name exists
			System.err.println(e.getMessage());
			inputStream = null;
			}
		return inputStream;
	}
	
	public static void main(String[] args) {
		ArrayList<Integer> buttList = new ArrayList<Integer>();
		
		DataParser<Integer> testButt = new DataParser<Integer>();
		
		buttList.add(1);
		
		
		testButt.a(buttList);
		
	}
	
	/*
	public static void main(String[] args)
	{
		InputStream	in;								// source of input data
	
		if (args.length >= 1) {
			in = getFileInputStream(args[0]);
			if (in == null)
				return;
			}
		else
			in = System.in;							// get typed input from user console
		
		Scanner		sc = new Scanner(in);			// gets one word at a time from input
		String		word;		
		DataParser<String> 	wordParse = new DataParser<String>();		
		
		//Test of sorting function.
		System.out.printf("CS261 - Assignment 1 - Lewis Notestine%n%n");
		while (sc.hasNext()) {						// is there another word?
			word = sc.next();						// get next word
			
			if (word.equals("---"))				// word that signals end of input
				break;
			
			// do something with each word in the input			
			wordParse.addNewData(word);
		}
		
		//Print the list.
		wordParse.printParsedList();
		
		System.out.printf("%nbye...%n");
	}*/
}
