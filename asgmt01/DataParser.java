import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**DataParser.java
 * Organizes data coming in from a sequential list into a "stem and leaf" data structure that 
 * counts the tokens of a particular data item and organizes them alphabetically; lists types of 
 * data items also according to the frequency of items that follow them.
 * Responsible for organizing data in "stems" and "leaves" according to their natural ordering.
 * */
public class DataParser<DataType extends Comparable<DataType>> {

	private ArrayList<DataStem<DataType>> 	stemList;
	private Integer 						currentStem; //holds the index of last stem used for word insertion. updated every time a word is processed.
	
	/**Ctor:  setting the currentStem index member to -1 means that there is no stem (state of having the first word).
	 * */
	public DataParser() {
		stemList = new ArrayList<DataStem<DataType>>();
		currentStem = -1;
	}
	
	/**Returns true if the parser has no stems.
	 * */
	public Boolean isEmpty() {
		return (!hasStem() && stemList.isEmpty());
	}
	
	
	
	public void a(Collection<Integer> students) {
		System.out.println("Butt butt butt");
		
	}
	
	/**Takes a new input data item (newData) and arranges it within the right order of stems (preceding data items)
	 * and leaves (following data items).  This is the primary responsibility of the class.
	 * */
	public void addNewData(DataType newData) {		
		Integer					findIndex;
		
		//binarySearch tells the right place to insert the item, even if it's not yet there.
		//This means we don't have to implement a separate sorting function.
		findIndex = Collections.binarySearch(stemList, (new DataStem<DataType>(newData)));
		
		if (findIndex < 0) { //item is not found. Use findIndex to insert it in the right place.
			findIndex = (-findIndex) - 1; 
			addStem(new DataStem<DataType>(newData), findIndex);
		}
		
		//Next, handle newWord as a leaf, but only if it's not the first word (i.e., currentStem has default value).
		if (hasStem()) {
			stemList.get(currentStem).insertNewLeaf(new DataLeaf<DataType>(newData));
		}
		
		//Finally, set the currentStem to be index of the stem we just created so following items know where to go...
		currentStem = findIndex;

		return;
	}
	
	
	/**Prints the items as currently stored in the dataParser. 
	 * Calls the "print" methods of each element of stemList.
	 * */
	public void printParsedList() {
		Iterator<DataStem<DataType>> stemIter;
		DataStem<DataType> curntStem;
		
		stemIter = stemList.iterator();
		while (stemIter.hasNext()) {
			curntStem = stemIter.next();
			curntStem.printStem();	
		}
		
		return;
	}
	
	/**Adds the stem to the list.  Assumes it's not already there.
	 * Typically called by addNewData().  
	 * Increments currentStem if it needs to be moved back (which is what happens with ArrayList.add())
	 * */
	protected void addStem(DataStem<DataType> newStem, Integer stemIndex) {
		if (!isEmpty() && stemIndex <= currentStem) { // have to update if we insert before the current stem, otherwise it moves out of place.
			currentStem++;
		}
		
		stemList.add(stemIndex, newStem);
		return;
	}
	
	/**Sorts the stems according to natural ordering.  Generally not necessary. 
	 * */
	protected void sortStems() {
		Collections.sort(stemList);
		return;
	}
	
	/**Returns true if there is no stem (currentStem is in its initialized state).  
	 * Generally true either after instantiation or after processing FIRST data item only.  
	 * */
	protected Boolean hasStem() {
		return currentStem != -1;
	}
		
}
