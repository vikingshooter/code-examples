import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.lang.Comparable; 

/**
 * DataStem.java: 
 * stores the root data and list of stems in an ArrayList, 
 * also has methods for handling decision-making for adding data items to the list 
 * 	(i.e., either increments existing leaf or adds new leaf).

 * */

public class DataStem<DataType extends Comparable<DataType>> implements Comparable<DataStem<DataType>> {
	
	private DataType						rootData;
	private ArrayList<DataLeaf<DataType>> 	leafList;
	
	/**Default DataStem Ctor.
	 * */
	public DataStem() {
		//rootData = "";
		leafList = new ArrayList<DataLeaf<DataType>>();
	}
	
	/**Ctor: stemData sets the rootData member.
	 * */
	public DataStem(DataType stemData) {
		rootData = stemData;
		leafList = new ArrayList<DataLeaf<DataType>>();
	}
	
	/**Returns the root data of the Stem
	 * */
	public DataType getRoot() {
		return rootData;
	}
	
	/**Returns true if there are no dataLeaves.
	 * */
	public Boolean isEmpty() {
		return (leafList.isEmpty());
	}
	
	/**Compares object based on the content of its rootData member.
	 * */
	public int compareTo(DataStem<DataType> cf) {		
		return this.rootData.compareTo(cf.rootData);
	}
	
	/**Inserts new word leaf into the list at alphabetically sorted position 
	 * OR increments the count of a leaf already representing the input data.
	 * */
	public void insertNewLeaf(DataLeaf<DataType> newLeaf) {
		Integer	findIndex;
		
		//binarySearch tells the right place to insert the item, even if it's not yet there.
		//This means we don't have to implement a separate sorting function.
		findIndex = Collections.binarySearch(leafList, newLeaf);
		
		if (findIndex < 0) { //item is not found. Use findIndex to insert it in the right place.
			findIndex = (-findIndex) - 1;				
			leafList.add(findIndex, newLeaf);
		} else {
			leafList.get(findIndex).countPlus();
		}
		
		return;
	}
	
	/**Prints the current word stem and calls printLeaf() on all its leaves. 
	 * */
	public void printStem() {
		Iterator<DataLeaf<DataType>> 	leafIter = leafList.iterator();
		DataLeaf<DataType> 				currentLeaf;
		
		if (!isEmpty()) {
			System.out.println(rootData + ":");
			
			while (leafIter.hasNext()) {
				currentLeaf = leafIter.next();
				currentLeaf.printLeaf();
			}
		}
		
		return;
	}
	
	/**Sorts the leaf list.  
	 * Under normal operation (i.e., of simply calling WordParser.processWord()
	 *  the list) this shouldn't be necessary because insertion is done using binarySearch.
	 * */
	protected void sortLeaves() {
		Collections.sort(leafList);
		return;
	}
	
}
