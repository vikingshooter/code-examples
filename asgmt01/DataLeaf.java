
/**DataLeaf.java
 * Stores count of leaves, intended to belong to a collection within the DataStem class.
 * Responsible for:
 * 	Keeping the dataType abstract so that any object can be the leaf data (so long as it implements Comparable.)
 * 	Keeping track of the name and count of tokens for a particular Data of an input.  
 * 	Printing the contents of a particular leaf. 
 * */

public class DataLeaf<DataType extends Comparable<DataType>> implements Comparable<DataLeaf<DataType>> {
	
	private DataType 	leafData;  //This is the parameterized class, which must itself implement the Comparable interface.
	private Integer  	leafCount;
	
	/**
	 * Default Ctor 
	 * */
	public DataLeaf() {
		setCount(0);
	};

	/**
	 * Ctor:  takes new name and sets the count.
	 * 			sets count to zero otherwise.  DataType is uninitialized unless given as param... 
	 * */
	public DataLeaf(DataType newName) {
		leafData = newName;
		setCount(1);
	}
	
	/**Gets the leafData
	 * */
	public DataType getData() {
		return leafData;
	}
	
	/**Sets the leafData
	 * */
	public void setData(DataType newName) {
		leafData = newName;
		return;
	}
	
	/**Gets the leafNumber of tokens of leafData as set by input.
	 * */
	public Integer getCount() {
		return leafCount;
	}
	
	/**
	 * Sets the count of leafs to be argument given in param.
	 * Protected setter because public arbitrary manipulation of count shouldn't generally happen.*/
	protected void setCount(Integer newCount) {
		leafCount = newCount;
		return;
	}
	
	/**Increments count of leaves by 1.
	 * */
	public void countPlus() {
		leafCount++;
		return;
	}
	
	/**Decrements count of leaves by 1.
	 * */
	public void countMinus() {
		leafCount--;
		return;
	}
	
	/**Compares dataLeaf based on the leafData member (dynamic comparison.)
	 * */
	public int compareTo(DataLeaf<DataType> cf) {
		return this.leafData.compareTo(cf.leafData);
	}
	
	/**Prints the leaf's data and count in the format specified in assgmt. 
	 * */
	public void printLeaf() {
		String outString = "   " + leafData + ", " + leafCount;
		System.out.println(outString);
		return;
	}
}
