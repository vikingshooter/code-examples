		<section id =footer class="mainBody">
			<strong>&copy; <?php echo date('Y')?> Lewis Notestine</strong>	
		</section>
	
		<script src="~/js/jquery-1.9.0.min.js"></script>
		<script src="~/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="~/js/main.js"></script>
		<script> 
			var $j = jQuery.noConflict();
		</script>
	</body>
</html>